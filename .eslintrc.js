module.exports = {
  extends: ["airbnb-base", "plugin:jest/recommended"],
  plugins: ["import", "jest"],
  rules: {
    "comma-dangle": 0
  }
  env: {
    node: true,
    "jest/globals": true,
  }
};
