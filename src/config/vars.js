const path = require('path');
const os = require('os');

// import env variables
require('dotenv-safe').load({
  path: path.join(__dirname, '../../.env'),
  sample: path.join(__dirname, '../../.env.example'),
  allowEmptyValues: true,
});

module.exports = {
  env: process.env.NODE_ENV,
  port: process.env.PORT || 3000,
  logs: process.env.NODE_ENV === 'production' ? 'combined' : 'dev',
  host: `http://${os.hostname()}:${process.env.PORT}`,
  root: path.normalize(`${__dirname}/../..`),
  privateKeyPath: path.join(__dirname, './certs/private.pem'),
  publicKeyPath: path.join(__dirname, './certs/public.pem'),
  jwtAlgorithm: process.env.JWT_ALGORITHM,
  jwtExpiration: process.env.JWT_EXPIRATION,
};
