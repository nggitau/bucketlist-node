// const appRoot = require('app-root-path');
const winston = require('winston');
const { root } = require('./vars');

const options = {
  file: {
    level: 'info',
    filename: `${root}/logs/app.log`,
    handleExceptions: true,
    json: true,
    maxSize: 5242880, // 5 MB
    maxFiles: 5,
    colorize: false,
    timestamp: true,
  },
  console: {
    level: 'debug',
    handleExceptions: true,
    json: false,
    colorize: false,
    prettyPrint: true,
    timestamp: true,
  },
};

const logger = winston.createLogger({
  transports: [
    new winston.transports.File(options.file),
    new winston.transports.Console(options.console),
  ],
  exitOnError: false,
});

logger.stream = {
  write: message => logger.info(message),
};

module.exports = logger;
