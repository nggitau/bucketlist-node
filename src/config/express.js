const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const compress = require('compression');
const cors = require('cors');
const helmet = require('helmet');

const { root } = require('./vars');
const routes = require('../api/routes/index');
const { errorHandler } = require('../api/middlewares/error-handler');
const logger = require('../config/winston');

const app = express();

// setup logging
app.use(morgan('common', { stream: logger.stream }));

// parse params and attach them to req.body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// gzip compression
app.use(compress());

// secure app by setting various http headers
app.use(helmet());

// enable CORS
app.use(cors());
app.use(express.static(`${root}/public`));

// enable authentication
// app.use(passport.initialize());
// passport.use('jwt', strategies.jwt);
// passport.use('google', strategies.google);
// passport.use('facebook', strategies.facebook);

// mount routes => still middleware
routes(app);

app.use(errorHandler);

module.exports = app;
