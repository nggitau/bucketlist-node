const swaggerJsDoc = require('swagger-jsdoc');
const { root } = require('./vars');

const swaggerDefinition = {
  info: {
    title: 'Bucketlist API',
    version: '1.0.0',
    description: 'A CRUD simple API to manage bucketlists',
  },
};

const options = {
  swaggerDefinition,
  apis: [
    `${root}/src/api/routes/*.js`,
    `${root}/src/api/swagger/*.yaml`,
  ],
};

const swaggerSpec = swaggerJsDoc(options);

module.exports = swaggerSpec;
