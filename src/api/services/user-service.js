const fs = require('fs');
const boom = require('boom');
const jwt = require('jsonwebtoken');
const { User } = require('../database/models');
const { privateKeyPath, jwtAlgorithm, jwtExpiration } = require('../../config/vars');

class UserService {
  static create(name, email, password) {
    return User.create({ name, email, password })
      .then(createdUser => createdUser)
      .catch(err => boom.boomify(err));
  }

  static findByEmail(email) {
    return User.findOne({
      where: {
        email,
      },
    });
  }

  static async generateToken({ id, name, email }) {
    const cert = fs.readFileSync(privateKeyPath);
    const userInfo = {
      id, name, email,
    };
    const token = await jwt.sign({ userInfo }, cert, {
      algorithm: jwtAlgorithm,
      expiresIn: jwtExpiration,
    });
    return token;
  }
}

module.exports = {
  UserService,
};
