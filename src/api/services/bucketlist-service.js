const { Bucketlist } = require("../database/models");

class BucketlistService {
  static create(id, title, description) {
    return Bucketlist.create({ title, description, userId: id });
  }

  static all(userId) {
    // Soon get pagination, limit, offset details
    return Bucketlist.findAll({
      where: {
        userId
      },
      attributes: {
        exclude: ["deletedAt", "userId"]
      }
    });
  }

  static show(bucketlistId, userId) {
    return Bucketlist.find({
      where: {
        userId,
        id: bucketlistId
      },
      attributes: {
        exclude: ["deletedAt", "userId"]
      }
    });
  }

  static async update(bucketlistId, userId, payload) {
    const bucketlist = await this.show(bucketlistId, userId);

    if (bucketlist) {
      return bucketlist.update({
        title: payload.title || bucketlist.title,
        description: payload.description || bucketlist.description
      });
    }
    return null;
  }

  static destroy(bucketlistId, userId) {
    return Bucketlist.destroy({
      where: {
        id: bucketlistId,
        userId
      }
    });
  }
}

module.exports = BucketlistService;
