const passport = require('passport');
const { ExtractJwt, Strategy } = require('passport-jwt');
const LocalStrategy = require('passport-local').Strategy;

const { User } = require('../database/models');
const { certPath } = require('../../config/vars');

passport.use('login', new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password',
}, async (email, password, done) => {
  try {
    const user = User.findOne({ email });
    if (!user) return done(null, false, { message: `No users found matching ${email}` });
    const validPassword = user.verifyPassword(password);
    if (!validPassword) return done(null, false, { message: 'Incorrect password' });
    return done(null, user);
  } catch (error) {
    return done(error);
  }
}));

passport.use(new Strategy({
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretKey: `${certPath}/bucketlist-cert`,
}, (jwtPayload, cb) => User.findById({ id: jwtPayload.id })
  .then(user => cb(null, user)
    .catch(err => cb(err)))));

