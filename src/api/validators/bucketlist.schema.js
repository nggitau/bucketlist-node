const Joi = require('joi');

module.exports = Joi.object({
  title: Joi.string()
    .min(2)
    .max(25)
    .required(),
  description: Joi.string()
    .min(10)
    .max(500),
});

