const Joi = require('joi');

const registerUserSchema = Joi.object({
  name: Joi.string()
    .min(2)
    .max(25)
    .required(),
  email: Joi.string()
    .email()
    .max(25)
    .required(),
  password: Joi.string()
    .min(6)
    .required(),
});

const loginUserSchema = Joi.object({
  email: Joi.string()
    .email()
    .max(25)
    .required(),
  password: Joi.string()
    .min(6)
    .required(),
});

module.exports = {
  registerUserSchema,
  loginUserSchema,
};
