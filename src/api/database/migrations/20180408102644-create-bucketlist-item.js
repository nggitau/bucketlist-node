module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.createTable('bucketlist_items', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      title: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      description: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      bucketlistId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'bucketlists',
          key: 'id',
          as: 'bucketlistId',
        },
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE,
      },
    }),
  down: queryInterface => queryInterface.dropTable('bucketlist_items'),
};
