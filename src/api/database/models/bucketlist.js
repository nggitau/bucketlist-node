module.exports = (sequelize, DataTypes) => {
  const Bucketlist = sequelize.define(
    'Bucketlist',
    {
      title: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      description: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      tableName: 'bucketlists',
      freezeTableName: true,
      timestamps: true,
      paranoid: true,
    },
  );
  Bucketlist.associate = (models) => {
    // belongs to one User
    Bucketlist.belongsTo(models.User, {
      foreignKey: 'userId',
      as: 'Owner',
      onDelete: 'CASCADE',
    });

    // has many BucketlistItems
    Bucketlist.hasMany(models.BucketlistItem, {
      foreignKey: 'bucketlistId',
      as: 'Items',
      onDelete: 'CASCADE',
    });
  };
  return Bucketlist;
};
