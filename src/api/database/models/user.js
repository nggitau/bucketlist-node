const bcrypt = require('bcryptjs');

const saltRounds = parseInt(process.env.SALT_ROUNDS, 10);

const hashPassword = async (user) => {
  const hash = await bcrypt.hash(user.password, saltRounds);
  user.password = hash; // eslint-disable-line no-param-reassign
};

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    'User',
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        validate: {
          isEmail: true,
        },
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      tableName: 'users',
      freezeTableName: true,
      timestamps: true,
      paranoid: true,
    },
  );

  User.associate = (models) => {
    // has many Bucketlists
    User.hasMany(models.Bucketlist, {
      foreignKey: 'userId',
      sourceKey: 'id',
      as: 'Bucketlist',
      onDelete: 'CASCADE',
    });
  };

  User.beforeCreate(user => hashPassword(user));

  User.prototype.verifyPassword = async (password, providedPassword) => {
    const isValid = await bcrypt.compare(password, providedPassword);
    return isValid;
  };

  return User;
};
