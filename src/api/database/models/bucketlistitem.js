module.exports = (sequelize, DataTypes) => {
  const BucketlistItem = sequelize.define(
    'BucketlistItem',
    {
      title: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      description: {
        type: DataTypes.STRING,
        allowNull: true,
      },
    },
    {
      tableName: 'bucketlist_items',
      freezeTableName: true,
      timestamps: true,
      paranoid: true,
    },
  );

  BucketlistItem.associate = (models) => {
    // belongs to one Bucketlist
    BucketlistItem.belongsTo(models.Bucketlist, {
      foreignKey: 'bucketlistId',
      as: 'Bucketlist',
      onDelete: 'CASCADE',
    });
  };

  return BucketlistItem;
};
