const router = require("express").Router();
const validator = require("express-joi-validation")({ passError: true });
const bucketlistController = require("../controllers/bucketlist/controller");
const BucketlistSchema = require("../validators/bucketlist.schema");
const isAuthenticated = require("../middlewares/auth");
const { asyncWrapper } = require("../middlewares/error-handler");

router
  /**
   * @swagger
   * /bucketlists:
   *    get:
   *      security:
   *        - Bearer: []
   *      tags:
   *         - Bucketlists
   *      summary: Fetch Bucketlists
   *      description: Returns all Bucketlists
   *      parameters: []
   *      responses:
   *        200:
   *          description: Operation successful
   *          schema:
   *            $ref: '#/definitions/Bucketlists'
   *        401:
   *          description: Access token is missing or invalid
   */
  .get(
    "/",
    isAuthenticated,
    asyncWrapper((req, res, next) => bucketlistController.all(req, res, next))
  )

  /**
   * @swagger
   * /bucketlists/{bucketlistId}:
   *    get:
   *      security:
   *        - Bearer: []
   *      tags:
   *         - Bucketlists
   *      summary: Get a specific Bucketlist
   *      description: Fetches the Bucketlist with the provided id
   *      parameters:
   *        - name: bucketlistId
   *          in: path
   *          description: The id of the Bucketlist to retrieve
   *          required: true
   *      responses:
   *        200:
   *          description: Operation successful
   *          schema:
   *            $ref: '#/definitions/Bucketlist'
   *        400:
   *          description: Bad request
   *        500:
   *          description: Server Error
   */
  .get(
    "/:bucketlistId",
    isAuthenticated,
    asyncWrapper((req, res, next) => bucketlistController.show(req, res, next))
  )

  /**
   * @swagger
   * /bucketlists:
   *    post:
   *      security:
   *        - Bearer: []
   *      tags:
   *         - Bucketlists
   *      summary: Create a Bucketlist
   *      description: Creates a Bucketlist
   *      parameters:
   *        - name: bucketlist
   *          in: body
   *          description: The Bucketlist to create
   *          required: true
   *          schema:
   *            $ref: '#/definitions/CreateBucketlist'
   *      responses:
   *        201:
   *          description: Operation successful
   *          schema:
   *            $ref: '#/definitions/Bucketlist'
   *        400:
   *          description: Bad request
   *        500:
   *          description: Server Error
   */
  .post(
    "/",
    isAuthenticated,
    asyncWrapper((req, res, next) =>
      bucketlistController.create(req, res, next)
    )
  )

  /**
   * @swagger
   * /bucketlists/{bucketlistId}:
   *    patch:
   *      security:
   *        - Bearer: []
   *      tags:
   *         - Bucketlists
   *      summary: Update a Bucketlist
   *      description: Updates the details of a Bucketlist
   *      parameters:
   *        - name: bucketlistId
   *          in: path
   *          description: The id of the Bucketlist to update
   *          required: true
   *        - name: update payload
   *          in: body
   *          description: The payload containing fields to update
   *          schema:
   *            $ref: '#/parameters/UpdateBucketlistPayload'
   *      responses:
   *        200:
   *          description: Operation successful
   *          schema:
   *            $ref: '#/definitions/Bucketlist'
   *        400:
   *          description: Bad request
   *        500:
   *          description: Server Error
   */
  .patch(
    "/:bucketlistId",
    isAuthenticated,
    asyncWrapper((req, res, next) =>
      bucketlistController.update(req, res, next)
    )
  )

  /**
   * @swagger
   * /bucketlists/{bucketlistId}:
   *    delete:
   *      security:
   *        - Bearer: []
   *      tags:
   *         - Bucketlists
   *      summary: Delete a Bucketlist
   *      description: Updates the details of a Bucketlist
   *      parameters:
   *        - name: bucketlistId
   *          in: path
   *          description: The id of the Bucketlist to delete
   *          required: true
   *          schema:
   *            $ref: '#/definitions/Bucketlist'
   *      responses:
   *        204:
   *          description: No content
   *        400:
   *          description: Bad request
   *        500:
   *          description: Server Error
   */
  .delete(
    "/:bucketlistId",
    isAuthenticated,
    asyncWrapper((req, res, next) =>
      bucketlistController.destroy(req, res, next)
    )
  )

  /**
   * @swagger
   * /bucketlists/{bucketlistId}/items:
   *    get:
   *      security:
   *        - Bearer: []
   *      tags:
   *         - Bucketlists
   *      summary: Fetch Bucketlist Items
   *      description: Returns all Bucketlist Items
   *      parameters:
   *        - name: bucketlistId
   *          in: path
   *          description: The Bucketlist to fetch items from
   *          required: true
   *      responses:
   *        200:
   *          description: Operation successful
   *          schema:
   *            $ref: '#/definitions/BucketlistItems'
   *        401:
   *          description: Unauthenticated
   */
  .get("/:bucketlistId/items", (req, res, next) => {
    res.status(200).send({ bucketlistId: req.params.bucketlistId });
  })

  /**
   * @swagger
   * /bucketlists/{bucketlistId}/items/{itemId}:
   *    get:
   *      security:
   *        - Bearer: []
   *      tags:
   *         - Bucketlists
   *      summary: Fetch one Bucketlist Items
   *      description: Returns one Bucketlist Items with provided id
   *      parameters:
   *        - name: bucketlistId
   *          in: path
   *          description: The id of the bucketlist to fetch items from
   *          required: true
   *        - name: itemId
   *          in: path
   *          description: The id of the item we want to retrieve
   *          required: true
   *      responses:
   *        200:
   *          description: Operation successful
   *          schema:
   *            $ref: '#/definitions/BucketlistItems'
   *        401:
   *          description: Unauthenticated
   */
  .get("/:bucketlistId/items/:itemId", (req, res, next) => {
    res.status(200).send(req.params);
  })

  /**
   * @swagger
   * /bucketlists/{bucketlistId}/items:
   *    post:
   *      security:
   *        - Bearer: []
   *      tags:
   *         - Bucketlists
   *      summary: Create a Bucketlist Item
   *      description: Creates a Bucketlist Item
   *      parameters:
   *        - name: bucketlistId
   *          in: path
   *          description: The id of the Bucketlist to post items to
   *          required: true
   *        - name: itemId
   *          in: path
   *          description: The id of the BucketlistItem to post items to
   *          required: true
   *      responses:
   *        201:
   *          description: Operation successful
   *          schema:
   *            $ref: '#/definitions/BucketlistItem'
   *        400:
   *          description: Bad request
   *        500:
   *          description: Server Error
   */
  .post("/:bucketlistId/items", (req, res, next) => {
    res.status(201).send(req.body);
  })

  /**
   * @swagger
   * /bucketlists/{bucketlistId}/items/{itemId}:
   *    patch:
   *      security:
   *        - Bearer: []
   *      tags:
   *         - Bucketlists
   *      summary: Update a Bucketlist
   *      description: Updates the details of a Bucketlist
   *      parameters:
   *        - name: bucketlistId
   *          in: path
   *          description: The id of the Bucketlist to update
   *          required: true
   *        - name: itemId
   *          in: path
   *          description: The id of the BucketlistItem to update
   *          required: true
   *        - name: update payload
   *          in: body
   *          description: The payload containing fields to update
   *          schema:
   *            $ref: '#/parameters/UpdateBucketlistPayload'
   *      responses:
   *        200:
   *          description: Operation successful
   *          schema:
   *            $ref: '#/definitions/Bucketlist'
   *        400:
   *          description: Bad request
   *        500:
   *          description: Server Error
   */
  .patch("/:bucketlistId/items/:itemId", (req, res, next) => {
    res.status(200).send(req.body);
  })

  /**
   * @swagger
   * /bucketlists/{bucketlistId}/items/{itemId}:
   *    delete:
   *      security:
   *        - Bearer: []
   *      tags:
   *         - Bucketlists
   *      summary: Delete a Bucketlist
   *      description: Updates the details of a Bucketlist
   *      parameters:
   *        - name: bucketlistId
   *          in: path
   *          description: The id of the Bucketlist to delete
   *          required: true
   *          schema:
   *            $ref: '#/definitions/Bucketlist'
   *        - name: itemId
   *          in: path
   *          description: The id of the BucketlistItem to delete
   *          required: true
   *          schema:
   *            $ref: '#/definitions/BucketlistItem'
   *      responses:
   *        204:
   *          description: No content
   *        400:
   *          description: Bad request
   *        500:
   *          description: Server Error
   */
  .delete("/:bucketlistId/items/:itemId", (req, res, next) => {
    res.status(204).send(req.body);
  });

module.exports = router;
