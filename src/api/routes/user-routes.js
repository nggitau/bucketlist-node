const router = require('express').Router();
const validator = require('express-joi-validation')({ passError: true });
const userController = require('../controllers/user/controller');
const { loginUserSchema, registerUserSchema } = require('../validators/user.schema');
const { asyncWrapper } = require('../middlewares/error-handler');

router
/**
 * @swagger
 * /auth/register:
 *    post:
 *      tags:
 *         - Auth
 *      summary: Create User
 *      description: Creates a new User with the provided parameters
 *      parameters:
 *        - name: user
 *          in: body
 *          description: The user to create
 *          required: true
 *          schema:
 *            $ref: '#/definitions/User'
 *      responses:
 *        201:
 *          description: User Created
 *        400:
 *          description: Bad request
 *        500:
 *          description: Server Error
 */
  .post(
    '/register',
    validator.body(registerUserSchema),
    asyncWrapper((req, res, next) => userController.registerUser(req, res, next)),
  )

/**
 * @swagger
 * /auth/login:
 *    post:
 *      tags:
 *         - Auth
 *      summary: Login User
 *      description: Logs in a user with their email and password
 *      parameters:
 *        - name: credentials
 *          in: body
 *          description: The user's login credentials
 *          required: true
 *          schema:
 *            $ref: '#/definitions/LoginUser'
 *      responses:
 *        200:
 *          description: Login Successful
 *        400:
 *          description: Bad request
 *        500:
 *          description: Server Error
 */
  .post(
    '/login',
    validator.body(loginUserSchema),
    asyncWrapper((req, res) => userController.loginUser(req, res)),
  );

module.exports = router;
