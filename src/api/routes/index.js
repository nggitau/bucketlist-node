const swaggerSpec = require('../../config/swagger');
const userRoutes = require('./user-routes');
const bucketlistRoutes = require('./bucketlist-routes');

const routes = (app) => {
  app.get('/api-docs.json', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    return res.send(swaggerSpec);
  });
  app.use('/auth', userRoutes);
  app.use('/bucketlists', bucketlistRoutes);
};

module.exports = routes;
