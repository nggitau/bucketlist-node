const request = require('supertest');
const app = require('../../config/express');
const { sequelize } = require('../database/models/index');

describe('POST /auth/register', () => {
  afterAll(() => sequelize.close());

  it('registers a new user', async () => {
    const payload = { name: 'Test Name', email: 'testname@example.com', password: 'pass123' };
    const res = await request(app)
      .post('/auth/register')
      .send(payload);
    expect(res.statusCode).toBe(201);
    expect(res.body.email).toBe('testname@example.com');
  });

  // these test Joi Request validation rules
  it('requires email to be provided', async () => {
    const payload = { name: 'Test Name', password: 'pass123' };
    const res = await request(app)
      .post('/auth/register')
      .send(payload);
    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe('ValidationError: child "email" fails because ["email" is required]');
  });

  it('requires name to be provided', async () => {
    const payload = { email: 'test@example.com', password: 'pass123' };
    const res = await request(app)
      .post('/auth/register')
      .send(payload);
    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe('ValidationError: child "name" fails because ["name" is required]');
  });

  it('requires password to be provided', async () => {
    const payload = { name: 'Test Name', email: 'test@example.com' };
    const res = await request(app)
      .post('/auth/register')
      .send(payload);
    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe('ValidationError: child "password" fails because ["password" is required]');
  });

  it('validates email format', async () => {
    const payload = { name: 'Test Name', email: 'testexample.com', password: 'pass123' };
    const res = await request(app)
      .post('/auth/register')
      .send(payload);
    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe('ValidationError: child "email" fails because ["email" must be a valid email]');
  });

  it('requires email to be unique', async () => {
    const payload = { name: 'Test Name', email: 'testunique@example.com', password: 'pass123' };
    await request(app)
      .post('/auth/register')
      .send(payload);
    const res = await request(app)
      .post('/auth/register')
      .send(payload);
    expect(res.statusCode).toBe(500);
  });
});
