const boom = require('boom');

// TODO: Move this to it's own middleware file
const asyncWrapper = fn => (req, res, next) => Promise.resolve(fn(req, res, next))
  .catch((err) => {
    if (!err.isBoom) return next(boom.badImplementation(err));
    return next(err);
  });

const errorHandler = (err, req, res, next) => {
  // handle request validation errors
  if (err && err.error && err.error.isJoi) {
    // we had a joi error, let's return a custom 400 json response
    return res.status(400).json({
      type: err.type, // will be "query" here, but could be "headers", "body", or "params"
      message: err.error.toString(),
    });
  } else if (err.isBoom) {
    // log the error if it's a server error // Winston or pino
    return res.status(err.output.statusCode).send(err.output.payload);
  }
  // pass on to another error handler
  return next(err);
};


module.exports = {
  asyncWrapper,
  errorHandler,
};

