const fs = require('fs');
const boom = require('boom');
const jwt = require('jsonwebtoken');
const { jwtAlgorithm, publicKeyPath } = require('../../config/vars');

const publicKey = fs.readFileSync(publicKeyPath);

async function isAuthenticated(req, res, next) {
  try {
    const token = req.header('Authorization');
    if (!token) return next(boom.unauthorized('Access token is missing'));
    // Go through jwt check to verify token. If invalid, throw boom invalid token 401 error
    const decoded = await jwt.verify(token.replace('Bearer ', ''), publicKey, { algorithms: [jwtAlgorithm] });
    req.user = decoded.userInfo;
    return next();
  } catch (error) {
    return next(boom.unauthorized('Access token is invalid'));
  }
}

module.exports = isAuthenticated;
