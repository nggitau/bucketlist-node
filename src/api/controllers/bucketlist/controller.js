const boom = require("boom");
const pick = require("lodash.pick");

const BucketlistService = require("../../services/bucketlist-service");

class BucketlistController {
  static async create(req, res, next) {
    try {
      const {
        user: { id },
        body: { title, description }
      } = req;
      const createdBucketlist = await BucketlistService.create(
        id,
        title,
        description
      );
      const responsePayload = pick(createdBucketlist, [
        "id",
        "title",
        "description",
        "createdAt",
        "updatedAt"
      ]);
      return res.status(201).send(responsePayload);
    } catch (error) {
      return next(boom.boomify(error));
    }
  }

  static async all(req, res, next) {
    try {
      const {
        user: { id }
      } = req;
      const allBucketlists = await BucketlistService.all(id);
      return res.status(200).send(allBucketlists);
    } catch (error) {
      return next(boom.boomify(error));
    }
  }

  static async show(req, res, next) {
    try {
      const {
        user: { id }
      } = req;
      const { bucketlistId } = req.params;
      const bucketlist = await BucketlistService.show(bucketlistId, id);
      if (!bucketlist)
        return next(
          boom.notFound(`User has no bucketlist with id: ${bucketlistId}`)
        );
      return res.status(200).send(bucketlist);
    } catch (error) {
      return next(boom.boomify(error));
    }
  }

  static async update(req, res, next) {
    try {
      const { id } = req.user;
      const { bucketlistId } = req.params;
      const updatedBucketlist = await BucketlistService.update(
        bucketlistId,
        id,
        req.body
      );
      if (!updatedBucketlist) {
        return next(
          boom.notFound(`User has no bucketlist matching id: ${bucketlistId}`)
        );
      }
      return res.status(200).send(updatedBucketlist);
    } catch (error) {
      return next(boom.boomify(error));
    }
  }

  static async destroy(req, res, next) {
    try {
      const { id } = req.user;
      const { bucketlistId } = req.params;
      await BucketlistService.destroy(parseInt(bucketlistId, 10), id);
      return res.status(204).send({});
    } catch (error) {
      return next(boom.boomify(error));
    }
  }
}

module.exports = BucketlistController;
