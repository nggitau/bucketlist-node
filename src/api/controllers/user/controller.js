const { UserService } = require('../../services/user-service');

const registerUser = async (req, res) => {
  const { name, email, password } = req.body;
  const createdUser = await UserService.create(name, email, password);
  if (createdUser.isBoom) throw new Error(createdUser.message);
  return res
    .status(201)
    .send({
      id: createdUser.id,
      name: createdUser.name,
      email: createdUser.email,
      createdAt: createdUser.createdAt,
      updatedAt: createdUser.updatedAt,
    });
};

const loginUser = async (req, res) => {
  const { email, password } = req.body;
  // get user with provided email
  const user = await UserService.findByEmail(email);
  if (!user) return res.status(400).send({ message: 'Invalid credentials' });
  // check passwords
  if (!user.verifyPassword(user.password, password)) return res.status(400).send({ message: 'Invalid credentials' });
  // if passords match, geneerate token and send back token to the user
  const token = await UserService.generateToken(user);
  return res.status(200).send({ 'jwt-token': token });
};

module.exports = {
  registerUser,
  loginUser,
};
