const app = require('./config/express');
const { port, env } = require('./config/vars');

app.listen(port, () => console.log(`App running on port ${port} (${env})`)); // eslint-disable-line no-console
